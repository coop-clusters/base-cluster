SHELL := /bin/bash

PWD=$(shell pwd)
STATUS=0
REMOTE_HOST=10.2.59.237
all: build

init:
	./init.sh

build: init
	make -f pub-make -k build
	chmod +x build/code/runtime/init.sh

build-with-basic-infra: init clone-basic-infra build
	echo "build with basic infra"

clone-basic-infra: init wget-orgs clone-exporters clone-basic-themes
	echo "infrastructure put in place"

clone-basic-themes: init
	make -f pub-make -k clone-basic-themes

clone-exporters: init
	make -f pub-make -k clone-exporters

wget-orgs: init
	make -f pub-make -k wget-orgs

clean:
	make -f pub-make clean
	(rm -rf build; \
        rm -rf ./exp-publisher; \
        rm -rf src/hw; \
        rm -rf src/sw; \
        rm -rf src/master-requirements; \
        rm -rf src/design; \
        rm -rf src/runtime; \
	rm -rf src/sitemap.org)

clean-infra:
	make -f pub-make clean-infra

export:
	rsync -a --progress build/code/runtime root@${REMOTE_HOST}:/root/
